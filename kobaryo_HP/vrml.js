// 幅、高さ
const width  = window.innerWidth;
const height = window.innerHeight;
// 
// レンダラの作成、DOMに追加
const renderer = new THREE.WebGLRenderer();
renderer.setSize(width, height);
renderer.setClearColor(0xf3f3f3, 1.0);
document.body.appendChild(renderer.domElement);

// シーンの作成、カメラの作成と追加、ライトの作成と追加
const scene  = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(50, width / height, 1, 100 );
camera.position.set(0, 1, 5);
const light  = new THREE.AmbientLight(0xffffff, 1);
scene.add(light);

var loader = new THREE.VRMLLoader();
loader.load("./g4_01.wrl", function(object) {
  object.scale.set(0.01,0.01,0.01);
  // object.position.set(0.1,0.1,0.1);

  scene.add(object);
});
// OrbitControls の追加
const controls = new THREE.OrbitControls( camera, renderer.domElement );
controls.userPan = false;
controls.userPanSpeed = 0.0;
controls.maxDistance = 500.0;
controls.maxPolarAngle = Math.PI * 0.495;
controls.autoRotate = true;
controls.autoRotateSpeed = .0;

// Parameter object
const PARAMS = {
  speed: 0.5,
};

// Pass the object and its key to pane
const pane = new Tweakpane();
pane.addInput(PARAMS, 'speed');

// レンダリング
const animation = () => {
  renderer.render(scene, camera);
  controls.update();
  requestAnimationFrame(animation);
};
animation();

